// ==UserScript==
// @name         hide_yj_search_ad
// @namespace    https://javelin.works/
// @version      0.1
// @description  Yahoo! JAPAN 検索結果の広告非表示
// @author       sanadan <jecy00@gmail.com>
// @include      https://search.yahoo.co.jp/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    document.getElementById("So1").style.display = "none";
    document.getElementById("So2").style.display = "none";
})();
